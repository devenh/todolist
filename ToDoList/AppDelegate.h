//
//  AppDelegate.h
//  ToDoList
//
//  Created by Deven Hariyani on 2/16/14.
//  Copyright (c) 2014 Deven Hariyani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
